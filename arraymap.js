const numbers = [5, 10, 15, 20, 25, 30];

const timesTen = numbers.map((number) => {
    return number * 10;
});

// console.log(timesTen);

/*
    1. Create an array called 'bools' of the following booleans:
        [true, true, false, true, false, false]
    2. Map over 'bools' and do the following:
        - if "true", return a random number in it's place
        - if "false", return 0
    3. Print out both arrays
    HINT: Google for "random number js" to find a random
          number function built-in to Javascript to use
*/

let bools = [true, true, false, true, false, false]

let getnumber = bools.map((boolsvalue) => {
    if (boolsvalue === true) {

        return Math.floor(Math.random() * 10);
    }
    return 0
})

console.log(getnumber)


/*
    1. Create an array called "prices" with the following values:
        [1.23, 19.99, 85.2, 32.87, 8, 5.2]
    2. Create a new array using map called "taxedPrices" that:
        - If the price is greater than 10, add 20% tax to it
        - Otherwise, do not add any tax
    3. Print out both arrays
*/


let price = [20, 19.99, 85.2, 32.87, 8, 5.2]

taxedprice = price.map((pricevalue) => {
    return Math.floor(pricevalue * 0.2)
})
console.log(price, taxedprice)



/*
    1. Create the following array called "items":
        ["light", "banana", "phone", "book", "mouse"]
    2. Create an new array called "plurals" that:
        - maps over "items" and adds an 's' to each one
    3. Print out both arrays
    4. Change it so if we see "mouse", we instead return "mice"
*/


let items = ["light", "banana", "phone", "book", "mouse"]

let plurals = items.map((item) => {
    return `${item}s`
})

console.log(plurals)


/*
    1. Create the following array called "row":
        [10, 20, 30, 40, 50]
    2. Create an new array called "matrix" that:
        - maps over "row" and return each item, but in an array
        (We should have a 2-dimensional array as a result)
    3. Print out both arrays
    "matrix" should look like this:
        [[10], [20], [30], [40], [50]]
*/


let row =  [10, 20, 30, 40, 50]
let matrix = row.map((number) => {
    return [number]
})


console.log(matrix)