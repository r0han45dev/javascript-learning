/* 

    what is promise?
        promise is way to handle async operation.
        promise is very usefull, when you are fetching large amount of data or file from server.
        promise is object that is not available yet but it will be resolved soon.
        promise object have 3 states which is listed below.
            1] pending:- the operation is on going and result is not yet received.
            2] fulfilled:- the operation has completed successfully, and promise will return the data
            3] reject:- the operation has failed because of an error occured. the promise will return an error object.

        promises are created using Promise constructor which takes two argument resolve and reject

*/



// const mypromiss = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve("ehiis")
//     }, 2000)
// })

// mypromiss.then((value) => { console.log(value) }).catch((err) => { console.log(err) })


const API = new Promise((resolve, reject) => {
    callback(resolve, reject)
})

function callback(resolve, reject) {
    setTimeout(() => {
        resolve("ehiis")
    }, 2000)
}

API.then((value) => { console.log(value) }).catch((err) => { console.log(err) })



const promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(10)
    }, 5000)
})

const promise2 = new Promise((resolve) => {
    setTimeout(() => {
        resolve(20)
    }, 5000)
})

Promise.all([promise1, promise2]).then((value) => {
    let value1 = value[0]
    let value2 = value[0]

    console.log(value1 * value2)
})