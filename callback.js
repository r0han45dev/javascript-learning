

/* 

    What is callback?
        Callback function is a function that is pass as an argument to the another function.
        The basic idea behind a callback function is that it allows us to pass a function as an argument to another function, and that function can then call the callback function when it has finished its work.
        This allows us to write code that executes asynchronously, without blocking the main threa

*/

// function doSomething(callback) {
//     console.log("Starting doSomething function");
//     callback();
//     console.log("Finishing doSomething function");
// }

// function logMessage() {
//     console.log("Message logged");
// }

// doSomething(logMessage);



function CalulationSum(a, b, callback) {
    const result = a + b
    callback(result)
}


function Sumofnumber(a,b) {
    CalulationSum(a, b, (result) => {
        console.log(result)
    })
}

Sumofnumber(1,2)
Sumofnumber(2,2)
Sumofnumber(3,2)