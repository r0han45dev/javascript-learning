
/* 

    What is synchronour in Js?
        Synchronous in js is a way to execute code line by line. Means executing code line by line.
        In synchronous until the before line of code is not get executed it will not execute or run next line of code.
        eg: shown in below
    
    point of observation.
      1] code will execute line by line
      2] If one of line is not get execute it will not allow to execute next line of code
      3] it block the operation until is get finish

*/

console.log("stp1")
console.log("stp2")
console.log("stp3")




/* 

    What is asynchronour in Js?
        Asynchronous is a way to execute some non-blocking operation. 
        Means it execute code in background while other code gets executed.
    
    point of observation.
      1] Asynchronous allow non-blocking operation
      2] asynchronous is usefull when you are retreving data from server which long time to return response.
      3
      
*/

// basic example
console.log("stp1")
setTimeout(() => {  
    console.log("stp2")
}, 1500);
console.log("stp3")
