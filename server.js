let data = [
    {
        "BInput": false,
        "BOutput": false,
        "LInput": false,
        "LOutput": false,
        "RInput": false,
        "ROutput": true,
        "TInput": false,
        "TOutput": false,
        "category": "startNode",
        "color": "#00e000",
        "editable": false,
        "id": "start",
        "key": "Start",
        "loc": "-209.99273576479362 -119.6962819961829",
        "name": "start",
        "node_name": "start",
        "text": "Start",
        "webhook_id": "5778a8fe-8c02-40de-bb16-045426ff4285"
    },
    {
        "BInput": false,
        "BOutput": false,
        "LInput": true,
        "LOutput": false,
        "RInput": false,
        "ROutput": true,
        "TInput": false,
        "TOutput": false,
        "category": "apiNode",
        "color": "#2ac79d",
        "editable": true,
        "id": "3f742aca-aa6e-4857-bdd1-4ed8e3054f93",
        "key": "API Node",
        "loc": "-48.49273576479362 -188.1962819961829",
        "name": "api",
        "node_name": "api",
        "text": "API Node",
        "webhook_id": "9a6d2a41-17db-46f0-b777-27038be2335c"
    },
    {
        "node_name": "decision",
        "text": "Decision Node",
        "key": "Decision Node",
        "color": "#2ac79d",
        "id": "7a93d2c6-9a2e-45fe-bf0a-0089210e6ea3",
        "webhook_id": "35f2e44b-1022-473e-98ec-9d28730e729d",
        "editable": true,
        "TInput": false,
        "TOutput": false,
        "RInput": false,
        "ROutput": true,
        "BInput": false,
        "BOutput": false,
        "LInput": true,
        "LOutput": false,
        "name": "decision",
        "inservices": [
            {
                "name": "input"
            }
        ],
        "outservices": [
            {
                "name": "true"
            },
            {
                "name": "false"
            }
        ],
        "category": "decisionNode",
        "loc": "27.00693139160154 -9.696281996182904"
    }
]
let from = [
    "start",
    "3f742aca-aa6e-4857-bdd1-4ed8e3054f93",
    "start"
]

let to = [
    "3f742aca-aa6e-4857-bdd1-4ed8e3054f93",
    "7a93d2c6-9a2e-45fe-bf0a-0089210e6ea3",
    "7a93d2c6-9a2e-45fe-bf0a-0089210e6ea3"
]

function DecisionnodeFrom(data, from, to) {
    // console.log(data,from,to)
    let source = data.filter((element,index) => element.id == from[index])[0];
    let destination = data.filter((element,index) => element.id == to[index])[0];
   

    console.log(source.name,destination.name)
}


DecisionnodeFrom(data, from, to)