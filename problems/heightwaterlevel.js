
const number = [1, 8, 6, 2, 5, 4, 8, 3, 7]

function maxWaterLevel(height) {
    let left = 0;
    let right = height[height.length - 1]
    let maxArea = 0;
    while (left < right) {
        let area = Math.min(height[left], height[right]) * (right - left)
        maxArea = Math.max(maxArea, area)
        if (height[left] < height[right]) {
            left++;
        } else {
            right--;
        }
    }
    return maxArea
}
console.log(maxWaterLevel(number))