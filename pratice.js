/*  
    Given nums = [2, 7, 11, 15], target = 9,

    Because nums[0] + nums[1] = 2 + 7 = 9,
    return [0, 1].


    input value:- nums = [2, 7, 11, 15]
    output value:- 9

    explanation return the sum of first to index.
*/



// leet code problems
// problem no 1
let salary = [4000, 3000, 1000, 2000]
function Salary(salary) {
    let maxSalary = Math.max(...salary)
    let minSalary = Math.min(...salary)
    let resultArray = salary.filter(element => {
        if (element != maxSalary && element != minSalary) {
            return element
        }
    });

    let finalresult = resultArray.reduce((previousvalue, currentvalue) => previousvalue + currentvalue, 0)
    return finalresult / resultArray.length
}

// problem 2
/* 
        Input: 
        n = 10 
        ["call","call","call"]
        Output: [10,11,12]
        Explanation: 
        counter() = 10 // The first time counter() is called, it returns n.
        counter() = 11 // Returns 1 more than the previous time.
        counter() = 12 // Returns 1 more than the previous time.


*/

/* 
    I've got possibility to learn on --n and n-- syntax.

    So if you also didn't really feel the difference before, here is an explanation for this:

    In JavaScript, the expressions --n and n-- are both decrement operators, but they behave differently.

    --n is a pre-decrement operator, which means that the value of n is decremented by 1 before any other expression is evaluated. So if n is initially equal to 5, then --n will evaluate to 4, and the value of n will be updated to 4.

    On the other hand, n-- is a post-decrement operator, which means that the value of n is decremented by 1 after any other expression is evaluated. So if n is initially equal to 5, then n-- will evaluate to 5, but the value of n will be updated to 4 after the expression has been evaluated.

*/
function createAdder(a) {
    return function add(b) {
        const sum = a + 1;
        return sum;
    }
}
const addTo2 = createAdder(2);
console.log(addTo2(5)) // 7

async function sleep(millis) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(millis)
        }, millis)
    })
}

let t = Date.now();
sleep(100).then(() => {
    console.log(Date.now() - t); // 100
});
