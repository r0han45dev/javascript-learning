// try {
//     const user = {
//         name: "alex",
//         age: 21,
//     }
//     console.log(user.profile.colour)
// } catch (error) {
//     console.log("something bad happened")
//     console.log(error)
// }


// exercise 
for (let i = 1; i <= 10; i++) {
    try {
        console.log(i)
        if (i === 5) {
            throw new Error("Boom!")
        }
    } catch (error) {
        console.log(error)
        continue;
    }
}


const user = { 
    name: "Misha",
    age: 22, 
    settings: { colour: "blue" } 
}
