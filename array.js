/*
    1. Create a variable called "multiple" and initialize it to 5
    2. Create an array with the numbers 10-15 (inclusive)
    3. Loop through the array and on each iteration:
     ,Multiply the number by the multiple and print out the result
     ,As you're printing, use this format (example):
            10 x 5 = 50
            11 x 5 = 55
            ...
            15 x 5 = 75
    BONUS: Try to see if you can make it so that all you need to change
    is the "multiple" variable and the program still works correctly.
*/
let multiple = 5
let numbers = [10, 11, 12, 13, 14, 15]
for (let i = 0; i < numbers.length; i++) {
    let result = numbers[i] * multiple
    console.log(`${numbers[i]} x ${multiple} = ${result}`)
}


/*
    Create a variable called "greeting" and initialize it to:
        "Hello, nice to meet you!"
    
    Use a loop to loop through this String (just like you would an array)
     ,On each loop iteration, print out what is at that index
    WHY does this happen?
    WHAT is a String, really?
*/

let greeting = "Hello, nice to meet you!"
for (let i = 0; i < greeting.length; i++) {
    console.log(`${greeting[i]} is at index ${i}`)
}



/*
    Create a variable called "total" that starts at 0
    Create an array called "grades" with the following values:
     ,55
     ,63
     ,82
     ,98
     ,91
     ,43
        
    Figure out how to print out the AVERAGE grade
    (This is the sum of all grades divided by the number of grades)
*/


let total = 0
let array = [55, 63, 82, 98, 91, 43]
for (let index = 0; index < array.length; index++) {
    total = total + array[index]
}
console.log(total / array.length)


/*
    Create a variable called "bakery" that points to an array
    Fill the array with the following String:
       ,"Cake"
       ,"Cookie"
       ,"Bread"
       ,"Scone"
    Print out bakery to make sure it has these 4 Strings in it.
    Create another variable called "myBakery" and assign it
    to "bakery" that we declared previously
    Add the following items to "myBakery":
       ,"Croissant"
       ,"Granola"
    Print out myBakery and bakery and observe the contents.
    WHY is this?
*/


let bakery = ["Cake", "Cookie", "Bread", "Scone"]
// console.log(bakery)
let myBakery = bakery
myBakery.push("Croissant"
    , "Granola")
console.log(bakery, myBakery)