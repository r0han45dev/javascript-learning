class TimeLimitedCache {
    constructor(key, value, duration) {
        this.key = key
        this.value = value
        this.duration = duration
        this.cache = new Map()
    }
    set() {
        const getcache = this.cache.get(this.key)
        if (getcache) {
            clearTimeout(getcache.timeout)
        }
        const timeout = setTimeout(() => { this.cache.delete(this.key) }, this.duration)
        const value = this.value
        this.cache.set(this.key, { value, timeout })
    }
    get() {
        return this.cache.has(this.key) ? this.cache.get(this.key).value : -1
    }
    count() {

    }
}

const TimeLimitedCacheinstance = new TimeLimitedCache(1, 42, 100)
// TimeLimitedCacheinstance.set()
console.log(TimeLimitedCacheinstance.get())